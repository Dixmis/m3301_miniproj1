"""
    Le crypteur/décripteur de Cesar
    ======================

    Application qui consiste à crypter/décrypter avec la technique de César.
"""
from crypt import *
from tkinter import *
from tkinter.messagebox import showinfo


def getTextArea():
    """
    Retourne le texte du textarea de la saisie utilisateur
    """
    return textarea.get("0.0", END).strip()


def crypt_string_callback():
    """
    Callback qui vide le textarea d'affichage, puis insert le contenu crypté
    dedans
    """
    textarea2.delete(1.0, END)
    textarea2.insert(END, crypt(
        getTextArea(), circulaire.get(), valueDecalage.get()))


def decrypt_string_callback():
    """
    Callback qui vide le textarea d'affichage, puis insert le contenu décrypté
    dedans
    """
    textarea2.delete(1.0, END)
    textarea2.insert(END, decrypt(
        getTextArea(), circulaire.get(), valueDecalage.get()))


def callback_about(event):
    """
    Show about page
    """
    showinfo(title="(Dé)crypteur de César",
             message="Application qui consiste à crypter/décrypter un texte \
             avec la technique de César. \
             Développée par Hugo Gennevée et Dimitri Bergès.")


fenetre = Tk()
fenetre.rowconfigure(0, weight=1)
fenetre.rowconfigure(1, weight=1)
fenetre.rowconfigure(2, weight=1)
fenetre.rowconfigure(3, weight=1)
fenetre.rowconfigure(4, weight=1)
fenetre.rowconfigure(5, weight=1)
fenetre.rowconfigure(6, weight=1)
fenetre.columnconfigure(0, weight=100)
fenetre.title("(Dé)crypteur de Cesar")
icon = PhotoImage(file='imgs/icone.png')
fenetre.iconphoto(False, icon)
fenetre.resizable(width=False, height=False)

logo = Canvas(fenetre, width=300, height=59, bd=0,
              highlightthickness=0, relief='ridge')
banner = PhotoImage(file='imgs/logo.png')
logo.grid(row=0, columnspan=3, pady=5)
logo.create_image(0, 0, image=banner, anchor='nw')
logo.bind("<Button-1>", callback_about)

textarea = Text(fenetre, wrap='word', height="15",
                borderwidth=2, relief="groove")
textarea.grid(column=0, row=1, sticky='nsew', rowspan=4)

textarea2 = Text(fenetre, wrap='word', height="15",
                 borderwidth=2, relief="groove")
textarea2.grid(column=0, row=5, sticky='nsew', rowspan=2)

valueDecalage = IntVar()
valueDecalage.set(8)

label_input_decalage = Label(fenetre, text="Décalage :")
label_input_decalage.grid(column=1, row=3)

input_Decalage = Entry(fenetre, textvariable=valueDecalage)
input_Decalage.grid(column=1, row=4)

circulaire = IntVar()

RadioButtonCirculaire = Radiobutton(
    fenetre, text="Circulaire", variable=circulaire, value=1)
RadioButtonCirculaire.grid(column=1, row=5)

RadioButtonNonCirculaire = Radiobutton(
    fenetre, text="Non circulaire", variable=circulaire, value=0)
RadioButtonNonCirculaire.grid(column=1, row=6)


button_crypter = Button(fenetre, text="CRYPTER", command=crypt_string_callback)
button_decrypter = Button(fenetre, text="DECRYPTER",
                          command=decrypt_string_callback)
button_crypter.grid(column=1, row=1)
button_decrypter.grid(column=1, row=2)

fenetre.mainloop()
