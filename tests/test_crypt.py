import unittest
from crypt import *

class TestCryptFunction(unittest.TestCase):

    def test_crypt_nocircular(self):
        self.assertEqual(crypt("ABCD"), "NOPQ")
        self.assertEqual(crypt("xyz"), "\x85\x86\x87")
        self.assertEqual(crypt("ABCD", 0, 13), "NOPQ")
        self.assertEqual(crypt("xyz", 0, 13), "\x85\x86\x87")
        self.assertEqual(crypt("ABCD", 0, 8), "IJKL")
        self.assertEqual(crypt("xyz", 0, 8), "\x80\x81\x82")

    def test_crypt_circular(self):
        self.assertEqual(crypt("ABCD", 1), "NOPQ")
        self.assertEqual(crypt("xyz", 1), "klm")
        self.assertEqual(crypt("ABCD", 1, 13), "NOPQ")
        self.assertEqual(crypt("xyz", 1, 13), "klm")
        self.assertEqual(crypt("ABCD", 1, 8), "IJKL")
        self.assertEqual(crypt("xyz", 1, 8), "fgh")

    def test_decrypt_nocircular(self):
        self.assertEqual(decrypt("NOPQ"), "ABCD")
        self.assertEqual(decrypt("\x85\x86\x87"), "xyz")
        self.assertEqual(decrypt("NOPQ", 0, 13), "ABCD")
        self.assertEqual(decrypt("\x85\x86\x87", 0, 13), "xyz")
        self.assertEqual(decrypt("IJKL", 0, 8), "ABCD")
        self.assertEqual(decrypt("\x80\x81\x82", 0, 8), "xyz")

    def test_decrypt_circular(self):
        self.assertEqual(decrypt("NOPQ", 1), "ABCD")
        self.assertEqual(decrypt("klm", 1), "xyz")
        self.assertEqual(decrypt("NOPQ", 1, 13), "ABCD")
        self.assertEqual(decrypt("klm", 1, 13), "xyz")
        self.assertEqual(decrypt("IJKL", 1, 8), "ABCD")
        self.assertEqual(decrypt("fgh", 1, 8), "xyz")